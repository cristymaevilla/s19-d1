// 8888888  ES6/ECMAScript2015 UPDATES  88888888888888

/*ECMAScript - is the standard that is used to create the implementation of the language e.g. JAVASCRIPT
-today's lastest version is Es9
-ES66 is an update ,commonly used for beginners of JS
  */

  const firstNum = 8 ** 2;
  console.log(firstNum);

 // EXPONENT ES6 Updates---------------------
 const secondNum= Math.pow(8,2);
   console.log(secondNum);

  // TEMPLATE LITERALS---------------------
//   -allows the use of string without concatenation
let name = "John";
// without template literal string;
console.log(">>>Message without template literals:");
let message = "Hello " + name + ". Welcome to programming!";
console.log(message);

// template literal string (BACKTICKS  ``)
console.log(`Message withtemplate literals with backticks:  ${message}`);
message = `Hello ${name}, Welcome to programming!`;
console.log(message);

console.log(`>>>YOU DONT need to use \n`);
// with backticks, you dont have to use \n just hit ENTER
const anotherMessage= `
${name} attended a "math competition".
He won it by 'solving 'the problem 8 **2 with the solution of ${secondNum}
`;
console.log(anotherMessage);

const interestRate = 0.1;
const principal = 1000;
console.log(`The interest on you savings account is  : ${principal*interestRate}`);

// ARRAY and OBJECT DESTRUCTURING =======================
/*Array destructuring
	- allows to unpack elements in arrays into distinct values*/

const fullName = ["Juan", "Dela", "Cruz"];
// Pre-Array destructuring
console.log(fullName [0]);
console.log(fullName [1]);
console.log(fullName [2]);

console.log(`Hello ${fullName[0]} ${fullName[0]} ${fullName[0]}! It's nice to meet you.`)
// WITH Array destructuring
console.log(`>>>>>WITH Array destructuring`);
const[firstName ,middleName ,lastName]=fullName;

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`)
console.log(`Hello ${firstName[0]} ${middleName[0]} ${lastName[0]}! It's nice to meet you.`)

// OBJECTS DESTRUCTURING===========================
/*	- allows us to unpack properties of objects into distinct variables */

const person = {
	givenName: "Jane",
	maidenName: "Smith",
	familyName: "Rogers"
}
// PRE-object destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

// OBJECT destructuring variables inside must be in order
console.log(`>>>>>WITH Object destructuring`);
const {givenName , maidenName, familyName} =person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(`Hello ${givenName}`)
function getFullName({givenName , maidenName, familyName}){
	console.log(`${givenName} ${maidenName} ${familyName}`)
}

getFullName(person);

// ARROW FUNCTION===============================
/*
BEFORE/conventional: 
	function nameFunction(){
		statements;}
ARROW FUNCTION -good for function na hindi gamitn ng paulit ulit
	const/let variable = (parameters) => {
		statement/s;
	}
*/
/*BEFORE:
function printFullname(firstName, middleName, lastName){
	console.log(`${givenName} ${middleName} ${lastName}`)
}
printFullname ("Taylor", "A", "Smith");*/
const printFullname = (firstName, middleName, lastName) => {
	console.log(`${firstName} ${middleName} ${lastName}`);
}
printFullname ("Taylor", "A", "Smith");

const students = ["John", "Jane", "Judy"];

console.log(`>>>>Pre-Arrow Function forEach:`)
students.forEach(function (student){
	console.log(`${student} is a student.`);
})

console.log(`>>>>Arrow Function forEach:`)
students.forEach( (student) =>{
	console.log(`${student} is a student.`);
});

/* ONE LINER
	const add=(x,y) => x+y;
	

*/
const add = (x,y)=> x+y;
console.log(add(8,7));

let total = add(1,6);
console.log(total);

// DEFAULT FUNCTION ARGUMENT VALUE-------------
console.log(`>>>>DEFAULT FUNCTION ARGUMENT VALUE:`)

const greet =( name= 'User')=> {
	return `Goodmorning, ${name}`;
}
console.log(greet ());
console.log(greet (`Peter`));

/*Class-Based object Blueprints-------------
-Allows creation/instantiation of objects using classes as blueprints
	SYNTAX:
	class className {
		constructor(objectPropertyA, objectPropertyB){
			this.objectPropertyA = objectPropertyA;
			this.objectPropertyB = objectsPropertyB;
		}
	}
*/
console.log(`>>>>Class-Based object Blueprints:`)

class Car{
	constructor(brand , name , year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}
const myCar = new Car ();
console.log(myCar);

myCar.brand="Porche";
myCar.name="Taycan";
myCar.year="2020";
console.log(myCar);

const myNewCar= new Car ("Tesla", "Model X", "2020");
console.log(myNewCar);